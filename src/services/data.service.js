import APP_CONFIG from './app.config'
import axios from 'axios';
class DataService {
  constructor() {
    this.config = new APP_CONFIG();
    axios.defaults.headers.common['Authorization'] = JSON.parse(localStorage.getItem("AUTH_KEY"))
    axios.defaults.headers.common['Access-Control-Allow-Origin']="*"
    // this.axios_config = {
    //   "Content-Type": "application/json",
    //   'Access-Control-Allow-Origin': '*',
    // }
  }
  async GetTransactions(minDate = "", maxDate = "") {
    axios.get(this.config.BASE_URL)
      .then(res => {
        return res.payload;
      })
  }
  async GetTransactionsByFilter(filter = "", minDate = "", maxDate = "") {

  }
  GetAuth() {
    axios.post(this.config.BASE_URL, this.config.DEFAULT_CREDS, this.axios_config).then(res => {
      localStorage.setItem("AUTH_KEY", { Authorization: `Bearer ${res.access_token}` });
    });
  }
  handleResponseError(response) {
    throw new Error("HTTP error, status = " + response.status);
  }
  handleError(error) {
    console.log(error.message);
  }
}
export default DataService;