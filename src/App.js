import React, {Component} from 'react';
import Analytics from './components/analytics';
import Configuration from './components/configuration';
import Notifications from './components/notifications';
import Issues from './components/issues';
import Transactions from './components/transactions';
import DataService from './services/data.service';
import './App.css';
import './assets/css/dash.css';
// import 'https://cdnjs.cloudflare.com/ajax/libs/material-design-iconic-font/2.2.0/css/material-design-iconic-font.min.css'
import {
  BrowserRouter,
  Switch,
  Route,
  Link
} from "react-router-dom";

class App extends Component {
  constructor(props) {
    super(props);
    this._service = new DataService();
    this.onLogin = this.onLogin.bind(this);
  }
  render() {
    return (
      <div className="App">
        <header className="App-header" id="header-alt">
          <a href="/" className="header-alt__trigger hidden-lg" data-rmd-action="block-open" data-rmd-target="#main__sidebar">
            <i className="zmdi zmdi-menu"></i>
          </a>
          <a href="index.html" className="header-alt__logo hidden-xs">Seerbit Support Manager</a>
          <ul className="header-alt__menu">
            <li>
              <a href="/">
                <i className="zmdi zmdi-search"></i>
              </a>
            </li>
            <li>
                <button className="btn btn-primary" type="button" onClick={() => this.onLogin()}>Login</button>
            </li>
            <li className="hidden-xs">
              <a href="../index.html"><i className="zmdi zmdi-home"></i></a>
            </li>
            <li className="header-alt__profile dropdown">
              <a href="/" data-toggle="dropdown">
                <img src="../img/demo/people/2.jpg" alt="" />
              </a>

              <ul className="dropdown-menu pull-right">
                <li><a href="/">Edit Profile</a></li>
                <li><a href="/">Account Settings</a></li>
                <li><a href="/">Other Settings</a></li>
                <li><a href="/">Logout</a></li>
              </ul>
            </li>
          </ul>
          <div className="header-alt__search-wrap">
            <form className="header-alt__search">
              <input type="text" placeholder="Search..." />
              <i className="zmdi zmdi-long-arrow-left"></i>
            </form>
          </div>
        </header>
        <main id="main">
          <BrowserRouter>
            <aside id="main__sidebar">
              <a className="hidden-lg main__block-close" href="" data-rmd-action="block-close" data-rmd-target="#main__sidebar">
                <i className="zmdi zmdi-long-arrow-left"></i>
              </a>
              <ul className="main-menu">
                <li>
                  <Link to="/">Analytics</Link>
                </li>
                <li><Link to="/transactions">Transactions</Link></li>
                <li><Link to="/issues">Issues</Link></li>
                <li><Link to="/notifications">Notifications</Link></li>
                <li><hr /></li>
                <li><Link to="/configuration">Configuration</Link></li>
              </ul>
            </aside>
            <div>
              <Switch>
                <Route path="/" component={Analytics} exact />
                <Route path="/transactions" component={Transactions} />
                <Route path="/issues" component={Issues} />
                <Route path="/notifications" component={Notifications} />
                <Route path="/configuration" component={Configuration} />
              </Switch>
            </div>
          </BrowserRouter>
        </main>
      </div>
    )
  }
  onLogin() {
    this._service.GetAuth();
  }
}
export default App;
