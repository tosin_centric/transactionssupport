import DataService from '../services/data.service';
import React, { Component } from 'react';
import GaugeChart from 'react-gauge-chart'
import { Chart } from "react-google-charts";
import { Bar } from 'react-chartjs-2';
class Analytics extends Component {
    constructor(props) {
        super(props);
        this._service = new DataService();
    }
    componentDidMount() {
        // this.GetTransactions()
    }

    GetTransactions() {
        this._service.GetTransactions().then(data => {
            this.setState({ items: data });
        }
        );
    }
    render() {
        return (
            <div className="container m-t-5" id="main__content">
                <div className="row">
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card__header">
                                <h1>Transactions Overview</h1>
                                <div>30% Failed</div>
                            </div>
                            <div className="card__body">
                                <GaugeChart id="gauge-chart5"
                                    nrOfLevels={420}
                                    arcsLength={[0.3, 0.5, 0.2]}
                                    colors={['#EC5146', '#EE963D', '#1E6BF4']}
                                    percent={0.37}
                                    arcPadding={0.02}
                                    arcWidth={0.09}
                                    textColor={'#000'}
                                />
                            </div>
                            <div className="card__footer">
                                <div className="row">
                                    <div className="col-md-4">
                                        <p className="m-0">Passed : Volume</p>
                                        <h4>5,000,000</h4>
                                    </div>
                                    <div className="col-md-4">
                                        <p className="m-0">Passed : Volume</p>
                                        <h4>5,000,000</h4>
                                    </div>
                                    <div className="col-md-4">
                                        <p className="m-0">Passed : Volume</p>
                                        <h4>5,000,000</h4></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card__header">
                                <h1>Detailed Overview</h1>
                                <div>30% Failed</div>
                            </div>
                            <div className="card__body">
                                <div className="row">
                                    <div className="col-sm-12">
                                        <div className="os-progress-bar primary">
                                            <div className="bar-labels">
                                                <div className="bar-label-left"><span>Progress</span><span className="positive">+12</span></div>
                                                <div className="bar-label-right"><span className="info">72/100</span></div>
                                            </div>
                                            <div className="bar-level-1" style={{ width: 100 + '%' }} >
                                                <div className="bar-level-2" style={{ width: 72 + '%' }} >
                                                    <div className="bar-level-3" style={{ width: 25 + '%' }} ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="os-progress-bar primary">
                                            <div className="bar-labels">
                                                <div className="bar-label-left"><span>Progress</span><span className="positive">+12</span></div>
                                                <div className="bar-label-right"><span className="info">72/100</span></div>
                                            </div>
                                            <div className="bar-level-1" style={{ width: 100 + '%' }} >
                                                <div className="bar-level-2" style={{ width: 72 + '%' }} >
                                                    <div className="bar-level-3" style={{ width: 25 + '%' }} ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="os-progress-bar primary">
                                            <div className="bar-labels">
                                                <div className="bar-label-left"><span>Progress</span><span className="positive">+12</span></div>
                                                <div className="bar-label-right"><span className="info">72/100</span></div>
                                            </div>
                                            <div className="bar-level-1" style={{ width: 100 + '%' }} >
                                                <div className="bar-level-2" style={{ width: 72 + '%' }} >
                                                    <div className="bar-level-3" style={{ width: 25 + '%' }} ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="os-progress-bar primary">
                                            <div className="bar-labels">
                                                <div className="bar-label-left"><span>Progress</span><span className="positive">+12</span></div>
                                                <div className="bar-label-right"><span className="info">72/100</span></div>
                                            </div>
                                            <div className="bar-level-1" style={{ width: 100 + '%' }} >
                                                <div className="bar-level-2" style={{ width: 72 + '%' }} >
                                                    <div className="bar-level-3" style={{ width: 25 + '%' }} ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="os-progress-bar primary">
                                            <div className="bar-labels">
                                                <div className="bar-label-left"><span>Progress</span><span className="positive">+12</span></div>
                                                <div className="bar-label-right"><span className="info">72/100</span></div>
                                            </div>
                                            <div className="bar-level-1" style={{ width: 100 + '%' }} >
                                                <div className="bar-level-2" style={{ width: 72 + '%' }} >
                                                    <div className="bar-level-3" style={{ width: 25 + '%' }} ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="os-progress-bar primary">
                                            <div className="bar-labels">
                                                <div className="bar-label-left"><span>Progress</span><span className="positive">+12</span></div>
                                                <div className="bar-label-right"><span className="info">72/100</span></div>
                                            </div>
                                            <div className="bar-level-1" style={{ width: 100 + '%' }} >
                                                <div className="bar-level-2" style={{ width: 72 + '%' }} >
                                                    <div className="bar-level-3" style={{ width: 25 + '%' }} ></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12">
                        <div className="card">
                            <div className="card__body">
                                <Chart
                                    height={'400px'}
                                    chartType="Bar"
                                    loader={<div>Loading Chart</div>}
                                    data={[
                                        
                                        ['Year', 'Sales', 'Expenses', 'Profit'],
                                        ['2014', 1000, 400, 200],
                                        ['2015', 1170, 460, 250],
                                        ['2016', 660, 1120, 300],
                                        ['2017', 1030, 540, 350],
                                    ]}

                                    options={{
                                        colors: ['#2D7BE5', '#A6C5F7', '#D2DDEC'],
                                        // Material design options
                                        chart: {
                                            title: 'Company Performance',
                                            subtitle: 'Sales, Expenses, and Profit: 2014-2017',
                                        },

                                        animation: {
                                            duration: 1000,
                                            easing: 'out',
                                            startup: true,
                                        },
                                    }}
                                    // For tests
                                    rootProps={{ 'data-testid': '2' }}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default Analytics